package com.geektechnique.redisexample.services;

import com.geektechnique.redisexample.commands.ProductForm;
import com.geektechnique.redisexample.domain.Product;
import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface ProductService {

    List<Product> listAll();

    Product getById(String id);

    Product saveOrUpdate(Product product);

    void delete(String id);

    Product saveOrUpdateProductForm(ProductForm productForm);
}
